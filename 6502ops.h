//
// Created by mgrus on 08/01/2022.
//

#ifndef C64EMU_6502OPS_H
#define C64EMU_6502OPS_H

#include <cstdint>
#include <cstring>
#include <cstdio>
#include <cstdlib>


#define _64K 1024*64
#define _4K 1024 * 4
#define _8K 1024 * 8
#define _1Kb 1000

class mos6510;

void ldx_imm(mos6510* cpu);
void ldx_zero(mos6510* cpu);
void ldx_zero_y(mos6510* cpu);
void ldx_abs(mos6510* cpu);
void ldx_abs_y(mos6510* cpu);


struct mos6510 {
    uint8_t ram[_64K];
    uint8_t char_rom[_4K];
    uint8_t basic_rom[_8K];
    uint8_t kernal_rom[_8K];
    uint8_t color_ram[_1Kb];
    uint16_t pc;
    uint8_t ac;
    uint8_t x;
    uint8_t y;
    uint8_t sr;
    uint8_t sp;
    int rdy = 1;
    int aec = 1;
    int cycles = 0;

    int opCycle = 0;
    int opTotalCycles = 0;

    uint8_t currentOp = 0;

    // Returns the value from memory
    // based on the current memory mapping
    // configuration at memory address
    // location 0x01.
    // This is only active for certain
    // address, such as 0xd000 which is shared
    // between i/o and char-rom.
    // E.g. if port01 bit 3 is set to 0,
    // we must interpret addresses >= 0xd000
    // as reads from char rom instead of the ram underneath.
    // As for e.g. the VICII, this mapping is again different.
    uint8_t readMemMapped(uint16_t inAddress) {
        int port01 = ram[0x01];
        // Check for char rom:
        if (inAddress >= 0xd000 && inAddress <= 0xDFFF) {
            if (port01 & (1 << 2) != 0) {
                // map char rom
                return char_rom[inAddress - 0xd000];
            } else {
                return ram[inAddress];
            }
        }
        // check for basic rom:
        else if (inAddress >= 0xA000 && inAddress <= 0xBFFF) {
            if ((port01 & 0) == 0) {
                return ram[inAddress];
            }
            if ((port01 & 0x01) == 1) {
                return ram[inAddress];
            }
            if ((port01 & 0x02) == 2) {
                return ram[inAddress];
            }
            if ((port01 & 0x03) == 3) {
                return basic_rom[inAddress - 0xA000];
            }
        }
        // check for the kernal:
        else if (inAddress >= 0xE000 && inAddress <= 0xFFFF) {
            if ((port01 & 0) == 0) {
                return ram[inAddress];
            }
            if ((port01 & 0x01) == 1) {
                return ram[inAddress];
            }
            if ((port01 & 0x02) == 2) {
                return kernal_rom[inAddress - 0xE000];
            }
            if ((port01 & 0x03) == 3) {
                return kernal_rom[inAddress - 0xE000];
            }
        }
        else {
            return ram[inAddress]
            ;
        }
    }

    /**
     * Currently writes are always written through
     * the memory location.
     * If there is rom mapped in, this is fine,
     * we only write to ram.
     *
     * @param value
     * @param location
     */
    void write(uint8_t value, uint16_t location) {
        ram[location] = value;

    }

    void handleCIAWrite(uint16_t location, uint8_t value) {

    }

    void dumpRegs() {
        printf("PC:  AC:  X:  Y:  SP:  RDY:  AEC:\n");
        printf("%#2x  %#2x   %#2x   %#2x %#2x   %#2x   %#2x\n",
               pc, ac, x, y, sp, rdy, aec);
    }

    void dumpSR() {

        uint8_t sr_c = (sr & 0x1) != 0 ? 1 : 0;
        uint8_t sr_z = ((sr & 0x2) != 0) ? 1 : 0;
        uint8_t sr_i = (sr & 0x4) != 0 ? 1 : 0;
        uint8_t sr_d = (sr & 0x8) != 0 ? 1 : 0;
        uint8_t sr_b = (sr & 0x10) != 0 ? 1 : 0;
        uint8_t sr_1 = (sr & 0x20) != 0 ? 1 : 0;
        uint8_t sr_v = (sr & 0x30) != 0 ? 1 : 0;
        uint8_t sr_n = (sr & 0x40) != 0 ? 1 : 0;

        printf("N V 1 B D I Z C\n");
        printf("%d %d %d %d %d %d %d %d\n", sr_n, sr_v, sr_1, sr_b, sr_d, sr_i, sr_z, sr_c);

    }

    void panicUnknownOp(uint8_t opcode) {
        // TODO handle ?!
        //printf("unknown opcode: %#2x\n", opcode);
        //exit(1);
    }

    void reset() {
        pc = 0;
        sr = 0;
        sp = 0;
        x = 0;
        y = 0;
        ac = 0;
        memset(ram, 0, _64K);
        memset(char_rom, 0, _4K);
        cycles = 0;

        // Set the processor ports to their default
        // values of the c64:
        ram[0x00] = 0x2F; // 00101111
        ram[0x01] = 0x37; // 00110111

        // Current io device number
        ram[0x13] = 0x00;       // keyboard for input and screen for output

        // Screen control register:
        // Set default value to
        // vertical raster scroll,
        // screenheight = 25 rows,
        // screen on,
        // textmode,
        // extended background mode off,
        // 00011011
        ram[0xD011] = 0x1B;
    }

    void exec() {
        // Do we need to fetch the actual opcode?
        if (opCycle == 0 ) {
            currentOp = ram[pc];
            opTotalCycles = getCyclesForOp(currentOp);
            opCycle++;
            return;
        } else {
            // Do the actual execution.
            // Our method of being
            // "cycle accurate" is to
            // consume all the cycles
            // and in the actual last cycle
            // do the complete excecution, i.e.
            // do the actual side-effect
            // in one go at the last op cycle.
            // We will see if this is good enough.
            if (opCycle < opTotalCycles) {
                opCycle++;
            } else {
                // Execute on last cycle through
                // this current op.
                switch (currentOp) {
                    case 0xa2: ldx_imm(this); break;
                    default:
                        panicUnknownOp(currentOp);
                }

                opCycle = 0;
                opTotalCycles = 0;
                currentOp = 0;
            }

        }


    }
#define DEBUG_
    /**
     * Simulates one cycle.
     */
    void step() {
        // Are we allowed to read or are we stunned
        // by the VIC?
        if (rdy) {
            exec();
        }

    #ifdef DEBUG
        dumpSR();
        dumpRegs();
    #endif
    }

    int getCyclesForOp(uint8_t op) {
        switch (op) {
            // ADC
            case 0x69: return 2;
            case 0x65: return 3;
            case 0x75: return 4;
            case 0x6D: return 4;
            case 0x7D: return 4;
            case 0x79: return 4;
            case 0x61: return 6;
            case 0x71: return 5;

            // AND
            case 0x29: return 2;
            case 0x25: return 3;
            case 0x35: return 4;
            case 0x2D: return 4;
            case 0x3D: return 4;
            case 0x39: return 4;
            case 0x21: return 6;
            case 0x31: return 5;

            // ASL
            case 0x0A: return 2;
            case 0x06: return 5;
            case 0x16: return 6;
            case 0x0E: return 6;
            case 0x1E: return 7;

            // BCC
            case 0x90: return 2;

            // BCS
            case 0xB0: return 2;

            // BEQ
            case 0xF0: return 2;

            // BIT:
            case 0x24: return 3;
            case 0x2C: return 4;

            // BMI
            case 0x30: return 2;

            //BNE
            case 0xD0: return 2;

            // BPL
            case 0x10: return 2;

            // BRK:
            case 0x00: return 7;

            // BVC
            case 0x50: return 2;

            // BVS
            case 0x70: return 2;

            // CLC
            case 0x18: return 2;

            // CLD
            case 0xD8: return 2;

            // CLI
            case 0x58: return 2;

            // CLV
            case 0xB8: return 2;

            // CMP:
            case 0xC9: return 2;
            case 0xC5: return 3;
            case 0xD5: return 4;
            case 0xCD: return 4;
            case 0xDD: return 4;
            case 0xD9: return 4;
            case 0xC1: return 6;
            case 0xD1: return 5;

            // CPX
            case 0xE0: return 2;
            case 0xE4: return 3;
            case 0xEC: return 4;

            // CPY
            case 0xC0: return 2;
            case 0xC4: return 3;
            case 0xCC: return 4;

            // DEC
            case 0xc6: return 5;
            case 0xD6: return 6;
            case 0xCE: return 6;
            case 0xde: return 7;

            // DEX
            case 0xca: return 2;

            // DEY
            case 0x88: return 2;

            // EOR
            case 0x49: return 2;
            case 0x45: return 3;
            case 0x55: return 4;
            case 0x4d: return 4;
            case 0x5d: return 4;
            case 0x59: return 4;
            case 0x41: return 6;
            case 0x51: return 5;

            // INC
            case 0xe6: return 5;
            case 0xf6: return 6;
            case 0xee: return 6;
            case 0xfe: return 7;

            // INX
            case 0xc8: return 2;

            // JMP
            case 0x4c: return 3;
            case 0x6c: return 5;

            // JSR
            case 0x20: return 6;

            // LDA
            case 0xa9: return 2;
            case 0xa5: return 3;
            case 0xb5: return 4;
            case 0xad: return 4;
            case 0xbd: return 4;
            case 0xb9: return 4;
            case 0xa1: return 6;
            case 0xb1: return 5;

            // LDX
            case 0xa2: return 2;
            case 0xa6: return 3;
            case 0xb6: return 4;
            case 0xae: return 4;
            case 0xbe: return 4;

            // LDY
            case 0xa0: return 2;
            case 0xa4: return 4;
            case 0xb4: return 4;
            case 0xac: return 4;
            case 0xbc: return 4;

            // LSR
            case 0x4a: return 2;
            case 0x46: return 5;
            case 0x56: return 6;
            case 0x4e: return 6;
            case 0x5e: return 7;

            // NOP
            case 0xea: return 2;

            // ORA
            case 0x09: return 2;
            case 0x05: return 3;
            case 0x15: return 4;
            case 0x0D: return 4;
            case 0x1d: return 4;
            case 0x19: return 4;
            case 0x01: return 6;
            case 0x11: return 5;

            // PHA
            case 0x48: return 3;

            // PHP
            case 0x08: return 3;

            // PLA
            case 0x68: return 4;

            // PLP
            case 0x28: return 4;

            // ROL
            case 0x2a: return 2;
            case 0x26: return 5;
            case 0x36: return 6;
            case 0x2e: return 6;
            case 0x3e: return 7;

            // ROR
            case 0x6A: return 2;
            case 0x66: return 5;
            case 0x76: return 6;
            case 0x6e: return 6;
            case 0x7e: return 7;

            // RTI
            case 0x60: return 6;

            // SBC
            case 0xe9: return 2;
            case 0xe5: return 3;
            case 0xf5: return 4;
            case 0xed: return 4;
            case 0xfd: return 4;
            case 0xf9: return 4;
            case 0xe1: return 6;
            case 0xf1: return 5;

            // SEC
            case 0x38: return 2;

            // SED
            case 0xf8: return 2;

            // SEI
            case 0x78: return 2;

            // STA
            case 0x85: return 3;
            case 0x95: return 4;
            case 0x8d: return 4;
            case 0x9d: return 5;
            case 0x99: return 5;
            case 0x81: return 6;
            case 0x91: return 6;

            // STX
            case 0x86: return 3;
            case 0x96: return 4;
            case 0x8e: return 4;

            // STY
            case 0x84: return 3;
            case 0x94: return 4;
            case 0x8c: return 4;

            // TAX
            case 0xaa: return 2;

            // TAY
            case 0xa8: return 2;

            // TSX
            case 0xba: return 2;

            // TXA
            case 0x8a: return 2;

            // TXS:
            case 0x9a: return 2;

            // TYA
            case 0x98: return 2;




        }
    }

};




#endif //C64EMU_6502OPS_H
