#include <iostream>
#include "6502ops.h"
#include "kxWindow.h"
#include "VICII.h"
#include <filesystem>

struct BinaryProgram {
    uint8_t* code;
    uint16_t len;
};

BinaryProgram* getTestBinary001() {
    uint8_t* code = new uint8_t[10];
    memset(code, 0, 10);
    //LDX imm
    code[0] = 0xa2;
    code[1] = 0x0;

    return new BinaryProgram{code, 2};
}

void loadProgramAt(uint16_t startAddress, BinaryProgram* binary, mos6510* cpu) {
    memcpy_s(cpu->ram, binary->len, binary->code, binary->len);

    // dump some memory locations
//    printf("\n");
//    for (int i = 0; i < 20; i++) {
//        printf("%d\t%#2x\n", i, cpu->ram[i]);
//    }

}


void exec(uint8_t opcode);
void dumpSR();
void dumpRegs();


void dumpCharactersOnScreen(kx::Window* window, mos6510* cpu) {

    int pixelOffsetX = 100;
    int pixelOffsetY = 100;
    // We assume characters are living at memory 0xd000 - 0xdfff
    // Lets print 5 characters
    for (int i = 0; i < 5 * 8; i += 8) {
        for (int cr = 0; cr < 8; cr++) {
            // Eeach character consists of
            // 8 rows, where each byte
            // gives the bitpattern of this "slice".
            // We set pixels at the places of 1.
            // Each bit encodes 1 pixel.
            uint8_t theByte =cpu->readMemMapped(0xd000 + (i + cr));
                    //cpu->ram[0xd000 + (i + cr)];
            for (int bit = 7; bit > -1; bit--) {
                int val = theByte & (1 << bit);
                if (val != 0) {
                    window->setPixel(pixelOffsetX, pixelOffsetY, {1, 1, 1, 1});
                }
                pixelOffsetX++;
            }

            pixelOffsetY++;
            pixelOffsetX = 100;
        }




    }
}

uint8_t* loadFileToBuf(const std::string& fileName, FILE** outfile, int& outlen) {
    const std::string cwd = std::filesystem::current_path().string();
    *outfile = fopen((cwd + "/" + fileName).c_str(), "rb");
    fseek(*outfile, 0, SEEK_END);
    outlen = ftell(*outfile);
    rewind(*outfile);
    uint8_t* buf = new uint8_t [outlen];
    fread_s(buf, outlen, 1, outlen, *outfile);
    return buf;
}

void loadFileToLocation(uint8_t* location, const std::string& fileName) {
    FILE* file = nullptr;
    int len = 0;
    uint8_t* buf = loadFileToBuf(fileName, &file, len);
    memcpy_s(location, len, buf, len);
    fclose(file);
    free(buf);

}

void loadFileToRam(mos6510* cpu, const std::string& fileName, int offset) {
    FILE* file = nullptr;
    int len = 0;
    uint8_t * buf = loadFileToBuf(fileName, &file, len);
    memcpy_s(&cpu->ram[offset], len, buf, len);

    free(buf);
    fclose(file);
}

int main(int argc, char** args) {
    printf("6502 cpu emulation starting.");
    mos6510 cpu;
    cpu.reset();

    loadProgramAt(0, getTestBinary001(), &cpu);
    loadFileToLocation(cpu.char_rom, "../assets/characters.901225-01.bin");
    loadFileToLocation(cpu.basic_rom, "../assets/basic.901226-01.bin");
    loadFileToLocation(cpu.kernal_rom, "../assets/kernal.901227-03.bin");

    kx::Window window(800, 600, false);

    VICII vic(&cpu, &window);

    bool run = true;

    // configure the processor port to map char rom into ram:
    // comment if you want to have the default basic setup.
    //cpu.ram[0x01] = 0x33; // 00110011

    // TODO write some boring tests
    // to check the memory mapping for char rom, basic rom, kernal rom
    // and i/o.

    bool singleStepMode = true;

    int step = 0;
    while (run) {

       auto events = window.pollEvents();
       for (auto e : events) {
           if (e == kx::WindowEvent::exit) {
               run = false;
           }
            if (singleStepMode) {
                if (e == kx::WindowEvent::keydown) {
                    printf("step/cycle: %d\n", step++);

                    // Both vic and 6510 are stepped in one cycle
                    // (phi2).
                    // We pretend this is phi2-low and step the vic:
                    vic.step();

                    // Here we pretend this is phi2-high and we step the
                    // 6510:
                    cpu.step();
                    cpu.dumpRegs();
                    cpu.dumpSR();


                }
            }
       }
       window.clearToColor({1, 0, 0, 0});
       window.present();
    }
    return 0;
}












