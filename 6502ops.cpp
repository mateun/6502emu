//
// Created by mgrus on 08/01/2022.
//

#include "6502ops.h"


void ldx_imm(mos6510* cpu) {
    uint8_t val = cpu->ram[cpu->pc + 1];
    cpu->x = val;
    if (val == 0) {
        cpu->sr |= 0x2;
    }

    // TODO test for negative

    cpu->pc += 2;
    cpu->cycles += 2;
}


void ldx_zero(mos6510 *cpu) {

}

void ldx_zero_y(mos6510 *cpu) {

}

void ldx_abs(mos6510 *cpu) {

}

void ldx_abs_y(mos6510 *cpu) {

}
