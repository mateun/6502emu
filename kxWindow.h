//
// Created by mgrus on 08/01/2022.
//

#ifndef C64EMU_KXWINDOW_H
#define C64EMU_KXWINDOW_H

#include <vector>

class SDL_Window;
class SDL_Renderer;



namespace kx {

    enum class WindowEvent {
        exit,
        keydown,
        keyup,
        mousebuttondown,
        mousebuttonup
    };

    struct Color {
        float r;
        float g;
        float b;
        float a;
    };

    class Window {

    public:
        Window(int w, int h, bool fullScreen);
        std::vector<WindowEvent> pollEvents();
        void clearToColor(Color col);
        void setPixel(int x, int y, Color col);
        void present();

    private:
        SDL_Window* window;
        SDL_Renderer* renderer;
    };

}




#endif //C64EMU_KXWINDOW_H
