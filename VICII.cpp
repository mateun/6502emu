//
// Created by mgrus on 08/01/2022.
//

#include "VICII.h"

/**
 * The workhorse of the VICII emulation.
 * We need to check the io registers,
 * read from video memory and
 * produce graphical output.
 */
void VICII::step() {
    checkGraphicsMode();
    render();
}



void VICII::checkCIAForBankSwitch() {
    uint8_t val = cpu->readMemMapped(0xDD00);
    if ((val & 0) == 0) {
        bank = 3;
    }

    if ((val & 1) == 1) {
        bank = 1;
    }
    if ((val & 2) == 2) {
        bank = 1;
    }
    if ((val & 3) == 3) {
        bank = 0;
    }
}

/**
 * As we are using only on of 4 banks with 16K at any time,
 * we use this function to return the actual start address
 * based on the current bank.
 * @return
 */
uint16_t VICII::getMemStart() {
    switch (bank){
        case 0: return 0x00;
        case 1: return 0x4000;
        case 2: return 0x8000;
        case 3: return 0xC000;
    }
    return 0;
}

void VICII::checkGraphicsMode() {
    uint8_t screenControlRegister = cpu->ram[0xd011];
    uint8_t screenCtrlReg2 = cpu->ram[0xd011];
    int ecm = screenControlRegister & (1 << 6);
    int bmm = screenControlRegister & (1 << 5);
    int mcm = screenCtrlReg2 & (1 << 4);
    if (ecm == 0 && bmm == 0 && mcm == 0) {
        bitmapMode = GraphicsMode::textMode;
    }

    if (ecm == 0 && bmm == 0 && mcm == 1) {
        bitmapMode = GraphicsMode::multicolorTextMode;
    }

    if (ecm == 0 && bmm == 1 && mcm == 0) {
        bitmapMode == GraphicsMode::bitmapMode;
    }
}

void VICII::render() {
    switch (bitmapMode) {
        case GraphicsMode::textMode: renderTextmode(); break;
        default: printf("graphics mode not implemented!\n"); break;
    }
}

void VICII::renderTextmode() {
    // We go through all the 40x25 rows of the screen ram
    // and look up the corresponding character from char-rom
    // and then render the char at the given position.
    for (int row = 0; row < 25; row++) {
        for (int col = 0; col < 40; col++) {
            uint8_t charIndex = cpu->ram[0x400 + col + (40 * row)];
            renderChar(charIndex, col, row);
        }
    }
}

void VICII::renderChar(uint8_t charIndex, int col, int row) {
    int pixelOffsetX = col;
    int pixelOffsetY = row;

    for (int cr = 0; cr < 8; cr++) {
        // Eeach character consists of
        // 8 rows, where each byte
        // gives the bitpattern of this "slice".
        // We set pixels at the places of 1.
        // Each bit encodes 1 pixel.
        uint8_t theByte = cpu->readMemMapped(0xd000 + (cr));
        //cpu->ram[0xd000 + (i + cr)];
        for (int bit = 7; bit > -1; bit--) {
            int val = theByte & (1 << bit);
            if (val != 0) {
                window->setPixel(pixelOffsetX, pixelOffsetY, {1, 1, 1, 1});
            }
            pixelOffsetX++;
        }

        pixelOffsetY++;
        pixelOffsetX = col;

    }
}