//
// Created by mgrus on 08/01/2022.
//

#ifndef C64EMU_VICII_H
#define C64EMU_VICII_H

#include "6502ops.h"
#include "kxWindow.h"

enum class GraphicsMode {
    textMode,
    multicolorTextMode,
    bitmapMode
};

class VICII {
public:
    VICII(mos6510* c, kx::Window* win) : cpu(c), window(win) {}
    void step();

private:
    uint16_t getMemStart();
    void checkCIAForBankSwitch();
    void checkGraphicsMode();
    void render();
    void renderTextmode();
    void renderMulticolorTextmode();
    void renderBitmpaMode();
    void renderChar(uint8_t charIndex, int col, int row);

private:
    mos6510* cpu;
    kx::Window* window;
    uint8_t bank = 0;
    GraphicsMode bitmapMode = GraphicsMode::textMode;

};


#endif //C64EMU_VICII_H
