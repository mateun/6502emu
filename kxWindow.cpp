//
// Created by mgrus on 08/01/2022.
//

#include "kxWindow.h"
#include <SDL.h>

kx::Window::Window(int w, int h, bool fullScreen) {

    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        SDL_Log("error initializing SDL2 %s", SDL_GetError());
        exit(1);
    }

    window = SDL_CreateWindow("C64 emu 0.0.1", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
                              w, h, SDL_WINDOW_SHOWN);

    renderer = SDL_CreateRenderer(window, 0, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

}

void kx::Window::clearToColor(kx::Color col) {
    SDL_SetRenderDrawColor(renderer, col.r * 255, col.g * 255, col.b * 255, col.a * 255);
    SDL_RenderClear(renderer);

}

void kx::Window::present() {
    SDL_RenderPresent(renderer);
}

void kx::Window::setPixel(int x, int y, kx::Color col) {
    SDL_SetRenderDrawColor(renderer, col.r*255, col.g*255, col.b*255, col.a * 255);
    SDL_RenderDrawPoint(renderer, x, y);
}

std::vector<kx::WindowEvent> kx::Window::pollEvents() {
    std::vector<kx::WindowEvent> events;
    SDL_Event ev;
    while (SDL_PollEvent(&ev)) {
        switch (ev.type) {
            case SDL_QUIT: events.push_back(kx::WindowEvent::exit);break;
            case SDL_KEYDOWN: events.push_back(kx::WindowEvent::keydown); break;
            case SDL_KEYUP  : events.push_back(kx::WindowEvent::keyup); break;
        }
    }

    return events;

}
